<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

<div class="container">
  <h2>Matrix multiplication</h2>
 
   <form action="" method="post" name="form1">
        {!! csrf_field() !!}
         <div class="form-group">
         {!! Form::label('row', 'Enter row for matrix 1') !!}

            {!! Form::text('row1','',['id'=>'row1']) !!}
           </div>

            <div class="form-group"> 
             {!! Form::label('col', 'Enter column for matrix 1') !!}
            {!! Form::text('col1', '',['id'=>'col1']) !!}
     </div>
           

             <div class="form-group">
            {!! Form::label('row', 'Enter row for matrix 2') !!}
             {!! Form::text('row2', '',['id'=>'row2']) !!}
        </div>


             <div class="form-group">    
            {!! Form::label('col', '  Enter column for matrix 2') !!}

            {!! Form::text('col2', '',['id'=>'col2']) !!}
           </div>
            
             <div class="form-group">
             {!! Form::button('generate matrix',['id'=>'submit']) !!}
           </div>

          <!--  <div class="form-group">
             {!! Form::submit('submit',['id'=>'submit']) !!}
           </div> -->
         
   </div>
          
    </div>


    <div id="tableDiv" style="margin-top: 40px;height: auto;">
        Matrix 1
    </div>

    <div id="tableDiv1" style="margin-top: 80px;height: auto;">
        Matrix 2
    </div>
  </form>
</div>

</body>

<script type="text/javascript">
  var mat1_col;
  var mat2_col;
  var mat1_row;
  var mat2_row;
             $("button").click(function(){

   
  
          mat1_row=$('#row1').val();
         mat1_col=$('#col1').val();
          mat2_row=$('#row2').val();
          mat2_col=$('#col2').val();

         if(mat1_col!=mat2_row)
         {

           alert("Matrix multiplication not possible");

          
         }

         else{

           var table_body = '<table id="matrix_table1">';
          for(var aa=0;aa<mat1_row;aa++){
            table_body+='<tr>';
            for(var bb=0;bb<mat1_col;bb++){
                table_body +='<td><input type="text" name="" size="2">';
                table_body +='';
                table_body +='</td>';
            }
            table_body+='</tr>';
          }
            table_body+='</table>';
           $('#tableDiv').html(table_body);
         



             var table_body1 = '<table id="matrix_table2">';
          for(var a=0;a<mat2_row;a++){
            table_body+='<tr>';
            for(var b=0;b<mat2_col;b++){
                table_body1 +='<td><input type="text" name="" size="2">';
                table_body1 +='';
                table_body1 +='</td>';
            }
            table_body1+='</tr>';
          }
            table_body1+='</table><input type="submit" value="multiply" name="submit" onclick="add_element_to_array();">';
           $('#tableDiv1').html(table_body1);
         }

        });


 
  var mat1_array = [];
    var mat2_array = [];

         function add_element_to_array(){  

    $('#matrix_table1 tr').has('td').each(function() {
        var arrayItem = {};
        $(this).closest('tr').find('input').each(function(index, item) {
            arrayItem[index] = $(this).val();
       
        });
        mat1_array.push(arrayItem);
        
    });
    console.log(mat1_array);

    $('#matrix_table2 tr').has('td').each(function() {
        var arrayItem2 = {};
        $(this).closest('tr').find('input').each(function(index, item) {
            arrayItem2[index] = $(this).val();
       
        });
        mat2_array.push(arrayItem2);
        
    });
    console.log(mat2_array);

    var m1=JSON.stringify(mat1_array);
var m2=JSON.stringify(mat2_array);

// var encodedUrl = encodeURIComponent(url);
   window.location= "{{route('show')}}?mat1_array=" +m1 +"&mat2_array="+m2 +"&mat1_row="+mat1_row +"&mat2_col="+mat2_col;
    }


</script>


</html>

   